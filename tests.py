import unittest
from pioc import PiOC, InvalidArgumentError


class Service(object):
    pass


class TestPiOC(unittest.TestCase):

    def setUp(self):
        self.di = PiOC()

    def test_keys(self):

        self.di['service1'] = 1
        self.di['service2'] = 2
        self.di['service3'] = 3

        keys = ['service1', 'service2', 'service3']

        self.assertListEqual(sorted(keys), sorted(self.di.keys()))

    def test_contains(self):

        self.di['service1'] = 1

        self.assertTrue('service1' in self.di)
        self.assertFalse('service0' in self.di)

    def test_delete(self):

        self.di['service1'] = 1
        self.di['service2'] = 2

        self.assertEquals(2, len(self.di.keys()))
        self.assertTrue('service1' in self.di)

        del self.di['service1']

        self.assertFalse('service1' in self.di)
        self.assertEquals(1, len(self.di.keys()))

    def test_set_number(self):

        self.di.set('service', 1)

        self.assertEqual(1, self.di.get('service'))

    def test_set_function(self):

        def func():
            return 1

        self.di.set('service', func)

        self.assertEqual(1, self.di.get('service'))

    def test_set_function_no_return(self):

        def func():
            pass

        self.di.set('service', func)

        self.assertEqual(None, self.di.get('service'))

    def test_set_object(self):

        service = Service()

        self.di.set('service', service)

        self.assertIs(service, self.di.get('service'))

    def test_set_function_object(self):

        def func():
            service = Service()
            return service

        self.di.set('service', func)

        self.assertIsNot(None, self.di.get('service'))

    def test_set_function_with_app(self):

        def func(app):
            service = Service()
            service.app = app
            return service

        self.di.set('service', func)

        self.assertIs(self.di, self.di.get('service').app)

    def test_get_item(self):

        self.di['service'] = 1

        self.assertEqual(1, self.di['service'])

    def test_get_not_existed_item(self):

        self.assertRaises(InvalidArgumentError, self.di.__getitem__, 'service')

    def test_extend_number_with_service_arg(self):

        self.di['service'] = 5

        @self.di.extend('service')
        def extend(service):
            return 10

        self.assertEqual(10, self.di.get('service'))

    def test_extend_number_with_no_service_arg(self):

        self.di['service'] = 5

        @self.di.extend('service')
        def extend():
            return 10

        self.assertEqual(10, self.di.get('service'))

    def test_extend_func(self):

        def func():
            return 5

        self.di.set('service', func)

        self.assertEqual(5, self.di['service'])

        @self.di.extend('service')
        def extend():
            return 10

        self.assertEqual(10, self.di['service'])

    def test_extend_func_object(self):

        def func():
            service = Service()
            service.num = 5
            return service

        self.di.set('service', func)

        self.assertEqual(5, self.di['service'].num)

        @self.di.extend('service')
        def extend(service):
            service.num = 10
            #return service

        self.assertEqual(10, self.di['service'].num)

    def test_extend_func_object_no_return(self):

        def func():
            service = Service()
            service.num = 5
            return service

        self.di.set('service', func)

        self.assertEqual(5, self.di['service'].num)

        @self.di.extend('service')
        def extend(service):
            service.num = 10

        self.assertEqual(10, self.di['service'].num)

    def test_extend_no_name(self):

        self.di.set('service', 1)

        with self.assertRaises(InvalidArgumentError):

            @self.di.extend()
            def extend(service):
                return 5

        self.assertEqual(1, self.di.get('service'))

    def test_extend_no_call(self):

        self.di.set('service', 1)

        with self.assertRaises(InvalidArgumentError):

            @self.di.extend
            def extend():
                return 5

        self.assertEqual(1, self.di.get('service'))

    def test_extend_raw(self):

        self.di.set('service', 1)

        def extend():
            return 5

        self.di.extend('service', extend)

        self.assertEqual(5, self.di.get('service'))

    def test_extend_raw_no_name(self):

        self.di.set('service', 1)

        def extend():
            return 5

        with self.assertRaises(InvalidArgumentError):
            self.di.extend(None, extend)

        self.assertEqual(1, self.di.get('service'))

    def test_inject_raw(self):

        def service():
            return 5

        self.di.inject('service', service)

        self.assertEqual(5, self.di.get('service'))

    def test_inject_raw_no_name(self):

        def service():
            return 5

        with self.assertRaises(InvalidArgumentError):

            self.di.inject(None, service)

        with self.assertRaises(InvalidArgumentError):

            self.assertEqual(5, self.di.get('service'))

    def test_inject_no_call(self):

        @self.di.inject
        def service():
            return 5

        self.assertEqual(5, self.di.get('service'))

    def test_inject_call(self):

        @self.di.inject()
        def service():
            return 5

        self.assertEqual(5, self.di.get('service'))

    def test_inject_with_name(self):

        @self.di.inject('my_service')
        def service():
            return 5

        with self.assertRaises(InvalidArgumentError):
            self.assertEqual(5, self.di.get('service'))

        self.assertEqual(5, self.di.get('my_service'))

    def test_inject_function_object(self):

        service1 = Service()
        service1.a = 5

        self.di['service1'] = service1

        @self.di.inject
        def service2(app):
            service1 = app.get('service1')
            service = Service()
            service.b = service1.a
            return service

        self.assertEqual(5, self.di.get('service1').a)
        self.assertEqual(5, self.di.get('service2').b)


