class InvalidArgumentError(ValueError):
    pass


class PiOC(object):
    """

    """

    def __init__(self):
        self.services = {}

    def __setitem__(self, key, value):
        self.set(key, value)

    def __getitem__(self, key):
        return self.get(key)

    def __contains__(self, item):
        return item in self.services

    def __delitem__(self, key):
        del self.services[key]

    def set(self, key, item):
        if callable(item):
            try:
                self.services[key] = item(self)
            except TypeError:
                self.services[key] = item()
        else:
            self.services[key] = item

    def get(self, key):
        if key not in self.services:
            raise InvalidArgumentError('Identifier "{0}" is not defined.'.format(key))
        return self.services[key]

    def keys(self):
        """
        Get container keys.
        """
        return self.services.keys()

    def inject(self, name=None, inject_func=None):
        """
        Inject service to container
        """

        if name is None and inject_func is None:
            # @app.inject()
            def decorator(func):
                return self.__inject_function(func)
            return decorator

        elif name is not None and inject_func is None:
            if callable(name):
                # @app.inject
                return self.__inject_function(name)
            else:
                # @app.inject('service') or @app.inject(name='service')
                def decorator(func):
                    return self.inject(name, func)
                return decorator

        elif name is not None and inject_func is not None:
            # app.inject('service', some_func)
            self.set(name, inject_func)
            inject_func._service_name = name
            return inject_func

        else:
            raise InvalidArgumentError("Unsupported arguments to PiOC.inject: ({0}, {1})".format(name, inject_func))

    def __inject_function(self, func):
        name = func.__name__
        return self.inject(name, func)

    def extend(self, name=None, extend_func=None):
        """

        """
        if name is None and extend_func is None:
            # @app.extend()
            def decorator(func):
                return self.__extend_function(func)
            return decorator

        elif name is not None and extend_func is None:
            if callable(name):
                # @app.extend
                return self.__extend_function(name)
            else:
                # @app.extend('service') or @app.extend(name='service')
                def decorator(func):
                    return self.extend(name, func)
                return decorator

        elif name is not None and extend_func is not None:
            # app.extend('service', some_func)
            if name not in self.services:
                raise InvalidArgumentError("Identifier '{0}' is not defined.".format(name))

            extend_func._service_name = name
            try:
                ext = extend_func(self[name])
            except TypeError:
                ext = extend_func()

            if ext is None:
                ext = self[name]

            self.set(name, ext)

            return extend_func

        else:
            raise InvalidArgumentError("Unsupported arguments to PiOC.extend: ({0}, {1})".format(name, extend_func))

    def __extend_function(self, func):
        name = func.__name__
        return self.extend(name, func)